pipeline {
    agent any

    environment {
		registry = "jonasbrum/app-dcw5" // ALTERAR
        registryCredential = "dockerhub_id"
        dockerImage = ''
    }

    stages {
    	stage('Clone Repository') {
    		steps {  
                git branch: "main", url: 'https://gitlab.com/jonasbrum/app-dcw5.git' // ALTERAR
			}
    	}
    	stage('Build Docker Image') {
            steps{
                script {
                    dockerImage = docker.build registry + ":dev"
                }
            }
        }
    	stage('Send image to Docker Hub') {
            steps{
                script {
                    docker.withRegistry( '', registryCredential) {
                        dockerImage.push()
                    }
                }
            }
        }
    	stage('Deploy') {
		    steps{
                step([$class: 'AWSCodeDeployPublisher',
                    applicationName: 'app-dcw5',
                    awsAccessKey: "AKIA2QERP2PXXBVA35TV", // ALTERAR
                    awsSecretKey: "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", // ALTERAR
                    credentials: 'awsAccessKey',
                    deploymentGroupAppspec: false,
                    deploymentGroupName: 'dcw-app-group', // ALTERAR
                    deploymentMethod: 'deploy',
                    excludes: '',
                    iamRoleArn: '',
                    includes: '**',
                    pollingFreqSec: 15,
                    pollingTimeoutSec: 600,
                    proxyHost: '',
                    proxyPort: 0,
                    region: 'us-east-1', // CHECAR
                    s3bucket: 'app-dcw5', // ALTERAR
                    s3prefix: '', 
                    subdirectory: '',
                    versionFileName: '',
                    waitForCompletion: true])
            }
        }
    	stage('Cleaning up') {
        	steps {
            	sh "docker rmi $registry:dev"
        	}
		}
    }
}